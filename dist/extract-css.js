'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.default = ExtractCSS;

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

Array.prototype.extendMap = function (callback, thisArg) {
	var T = void 0,
	    k = void 0;

	if (this == null) {
		throw new TypeError(' this is null or not defined');
	}

	var O = Object(this);
	var len = O.length >>> 0;

	if (typeof callback !== 'function') {
		throw new TypeError(callback + ' is not a function');
	}

	if (arguments.length > 1) {
		T = thisArg;
	}

	var A = new Array(len);
	k = 0;

	while (k < this.length) {
		var kValue = void 0,
		    mappedValue = void 0;

		if (k in O) {
			kValue = O[k];
			mappedValue = callback.call(T, kValue, k, O);
			A[k] = mappedValue;
		}

		k++;
	}

	return A;
};

//export default function ExtractCSS(inputs) {
function ExtractCSS(inputs, callback) {
	/*	inputs = Object.assign({
 		element: undefined,
 		class: undefined,
 		id: undefined,
 		property: undefined,
 		feather: false
 	}, inputs);
 */
	try {
		var sheets = Array.from(document.styleSheets);
		var animations = {};
		var results = [];
		var weightedResults = {};

		sheets.map(function (sheet) {
			if (!sheet.cssRules) throw new ExtractError(405, "No CSS Rules!\nIn certain browsers, CSS rules cannot be accessed if they are loaded from local disk or cross origin.");
			var cssSheet = Array.from(sheet.cssRules);
			cssSheet.extendMap(function (rule) {
				var parseText = rule.selectorText;

				if (rule.type === 4) {
					// @media
					cssSheet.push.apply(cssSheet, _toConsumableArray(Array.from(rule.cssRules)));
				}

				if (rule.type === 7) {
					// @keyframes
					animations[rule.name] = rule.cssRules;
				}

				if (parseText) {
					var selectors = rule.selectorText.split(' ');
					selectors.map(function (selector) {
						var searchObject = ParseCSS(selector);
						searchObject.styles = {};

						if (rule.parentRule) {
							// If @media add conditions to object
							if (searchObject.conditions) {
								searchObject.conditions.push(rule.parentRule.conditionText.split(/(?:and)|\s|\((.+)\)/).filter(function (a) {
									if (a) return a;
								}));
							} else {
								searchObject.conditions = rule.parentRule.conditionText.split(/(?:and)|\s|\((.+)\)/).filter(function (a) {
									if (a) return a;
								});
							}
						}

						var aMatch = undefined;
						Object.keys(inputs).map(function (key) {
							if (["element", "class", "id", "pseudoelement", "pseudoclass"].includes(key.toLowerCase()) && (aMatch === undefined || aMatch)) {
								if (inputs[key] === searchObject[key] || inputs[key] === "" && !searchObject[key]) aMatch = true;else aMatch = false;
							}
						});

						var weight = 0;
						if (inputs.feather && !aMatch) {
							var multi = 0;

							inputs.element === searchObject.element && inputs.element != "" && !searchObject.class ? weight += 1 : null;
							inputs.class && inputs.class === searchObject.class ? multi += 1 : null;
							inputs.id && inputs.id === searchObject.id ? multi += 1 : null;

							if (inputs.class && inputs.class === searchObject.class || inputs.id && inputs.id === searchObject.id) {
								weight += 3 * multi;
								inputs.element === searchObject.element ? weight += 6 * multi : null;
								if (inputs.element != searchObject.element) {
									searchObject.element === "" && inputs.element != "" ? weight += 2 * multi : null;
									searchObject.element != "" && inputs.element === "" ? weight += 1 * multi : null;
								}

								if (inputs.pseudoElement) {
									inputs.pseudoElement === searchObject.pseudoElement ? weight += 2 * multi + 1 : null;
									!searchObject.pseudoElement ? weight += 1 * multi : null;
								}

								if (inputs.pseudoClass) {
									inputs.pseudoClass === searchObject.pseudoClass ? weight += 2 * multi + 1 : null;
									!searchObject.pseudoClass ? weight += 1 * multi : null;
								}
							}
						}

						if (aMatch || weight > 0) {
							Array.from(rule.style).map(function (style) {
								// Gather registered styles
								if (style && style.toString().split('-')[0] != "") {
									if (["animation", "border", "margin", "padding", "background"].includes(style.toString().split('-')[0])) {
										searchObject.styles[style.toString().split('-')[0]] = rule.style[style.toString().split('-')[0]];
										if (style.toString() === "animation-name") {
											// For animations, gather the registered @keyframes
											if (animations[rule.style[style.toString()]]) {
												searchObject.styles.animationFrames = BreakdownAnimation(animations[rule.style[style.toString()]]);
											} else {
												if (inputs.property && inputs.property === "animation") {
													throw new ExtractError(404, "Animation has no frames");
												} else {
													searchObject.styles.animationFrames = { 404: "Animation has no frames" };
												}
											}
										}
									} else {
										searchObject.styles[style.toString()] = rule.style[style.toString()];
									}
								}
							});

							if (inputs.property) {
								if (!searchObject.styles[inputs.property.toLowerCase()]) {
									aMatch = false;
									weight = 0;
								} else {
									searchObject.property = {};
									searchObject.property[inputs.property.toLowerCase()] = searchObject.styles[inputs.property.toLowerCase()];
									if (inputs.property.toLowerCase() === "animation") {
										searchObject.property.animationFrames = searchObject.styles.animationFrames;
									}
								}
							}
						}

						if (aMatch) {
							results.push(searchObject);
						} else if (weight > 0) {
							weightedResults[weight] = searchObject;
						}
					});
				}
			});
		});

		if (results.length === 0) {
			var weightedList = Object.keys(weightedResults);
			weightedList.sort(function (a, b) {
				return b - a;
			});
			var weightedFinal = [];
			weightedList.map(function (weight) {
				weightedFinal.push(weightedResults[weight]);
			});
			if (weightedFinal.length === 0 || !inputs.feather) {
				callback(undefined);
				//				return undefined;
			} else {
				if (weightedFinal.length === 1 || inputs.firstOnly) {
					callback(weightedFinal[0]);
					//					return weightedFinal[0];
				} else {
					callback(weightedFinal);
					//					return weightedFinal;
				}
			}
		} else if (results.length === 1 || inputs.firstOnly) {
			callback(results[0]);
			//			return results[0];
		} else {
			callback(results);
			//			return results;
		}
	} catch (e) {
		//		console.error(`ERROR! ${e.status}: ${e.message}`);
		callback({ status: e.status, message: e.message });
		//		return {status: e.status, message: e.message};
	}
}

function ParseCSS(input) {
	var css = input.split(/(::|\[.+\]|,|:|\s|\.|#)/);
	var searchObject = {};

	if (css) {
		css.map(function (segment, index) {
			var attrib = [];
			switch (index % 2) {
				case 0:
					switch (index) {
						case 0:
							// First segment, if not empty, will always be the element.
							searchObject.element = segment;
							break;
						default:
							// Even segments, that are not first, can be a variety of things;
							// class, id, pseudo-class, pseudo-element
							switch (css[index - 1]) {
								case ",":
									// standard separator; not counted.
									break;
								case ".":
									// class
									searchObject.class = searchObject.class === undefined ? segment : searchObject.class;
									break;
								case "#":
									// id
									searchObject.id = searchObject.id === undefined ? segment : searchObject.id;
									break;
								case ":":
									// pseudo-class
									searchObject.pseudoClass = searchObject.pseudoClass === undefined ? segment : searchObject.pseudoClass;
									break;
								case "::":
									// pseudo-element
									searchObject.pseudoElement = searchObject.pseudoElement === undefined ? segment : searchObject.pseudoElement;
									break;
								default:
									attrib = css[index - 1].split(/([\[\]])/);
									if (attrib[1] === "[" && attrib[3] === "]") {
										searchObject.attributeSelector = searchObject.attributeSelector === undefined ? attrib[2] : searchObject.attributeSelector;
									}
							}
							break;
					}
					break;
			}
		});
	}

	return searchObject;
}

function BreakdownAnimation(animationObject) {
	var animationSet = {};
	var animationList = Array.from(animationObject);

	animationList.map(function (keyFrame) {
		var keyText = keyFrame.keyText;
		animationSet[keyText] = {};
		var keyStyles = Array.from(keyFrame.style);
		keyStyles.map(function (style) {
			if (style) {
				animationSet[keyText][style.toString()] = keyFrame.style[style.toString()];
			}
		});
	});

	return animationSet;
}

function ExtractError(code, message) {
	this.status = code;
	this.message = message;
	this.toString = function () {
		return this;
	};
}