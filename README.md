# js-extract-css

Extracts CSS data and passes it upon request to your JavaScript.



## Installation

- `npm install --save-dev https://Palelion@bitbucket.org/Palelion/js-extract-css.git`


## Instructions

`import ExtractCSS from 'js-extract-css';`

`ExtractCSS({...Arguments});`

Returns either a single Object literal with the desired parameters, or an array of Objects, in case more CSS entries match the submitted queries.

Returned object(s):  
`{`  
`  element: HTML element,`  
`  class: CSS class,`  
`  id: HTML ID,`  
`  pseudoElement: CSS pseudo-element,`  
`  pseudoClass: CSS pseudo-class,`  
`  conditions: Array of @media conditions if applicable,`  
`  animationFrames: Object holding @keyframe objects returned if property is an animation,`  
`  property: Object holding just the requested property and its value,`  
`  styles: Object holding all assigned properties and values for the CSS object`  
`}`  


## Arguments

### element (optional)
specifies the CSS element type to look for.  
Example: `'div', 'p', 'h2', 'a', ..`  
If omitted, all element types matching the other criteria will be returned.  
If you want only non-element specific objects (i.e. `.class { ..`) don't omit, but pass empty string.  
Example: `ExtractCSS({element: "", class: "class"});`

### id (optional)
specifies the ID `#` to look for.  
Example: `'div#main'`  

### class (optional)
specifies the CSS class `.` to look for.  
Example: `'p.login-message'`  

### pseudoClass (optional)
specifies the CSS pseudo-class `:` to look for.  
Example: `'a:hover'`  

### pseudoElement (optional)
specifies the CSS pseudo-element `::` to look for.  
Example: `'section::first-letter'`  

### property (optional)
specifies desired CSS property.  
Example: `'color', 'background-image', 'animation', ..`  
f omitted, all styles will be returned in `styles` key, and `property` will not be returned.  

### feather (optional; default `false`)
reduces sensitivity.  
If `true`, return will be an array of several near-matches in order of relevance.  

### firstOnly (optional; default `false`)
forces the return of only first result, no matter how many matches are found.  


## Use case example

- Find the desired color for paragraph (<P\>) elements with a CSS class of 'chart'.

`const paragraphColor = ExtractCSS({element: "p", class: "chart", property: "color"});`  
`const extractedColor = paragraphColor.property.color;`


- Get the animation @keyframes for <DIV\> element with the CSS class 'roll-in'.

`const animData = ExtractCSS({element: "div", class: "roll-in", property: "animation"});`  
`const animFrames = paragraphColor.property.animationFrames;`


## Error messages

### 404 - Animation has no frames
Returned in an animation is requested, but the named animation has no @keyframes object in the CSS documents.

### 405 - No CSS rules
Returned in the script cannot access CSS document, or if no CSS document is present.  
**Note:** Certain browsers (such as Chrome, Opera) will not currently allow scripts to access stylesheets if these are loaded from local disk, or from cross server. If you are getting this error during local development, you may need to set up a HTTP server. If you get this error in deployment, sadly there is little to be done, until this condition is changed in affected browsers.