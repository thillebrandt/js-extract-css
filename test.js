import ExtractCSS from './src/extract-css';

console.time('Extract');
console.dir(ExtractCSS({class: "roll", property: "animation"}).property);
console.dir(ExtractCSS({element: "p", class: "pklasse", property: "color"}));
console.dir(ExtractCSS({element: "h2"}));
console.dir(ExtractCSS({class: "ryll", property: "width"}));
console.dir(ExtractCSS({class: "pklasse", property: "color"}));
console.dir(ExtractCSS({element: "", class: "pklasse", property: "color"}));
console.dir(ExtractCSS({class: "pklasse", property: "color", firstOnly: true}));
console.timeEnd('Extract');

//console.time('Feather');
//console.dir(ExtractCSS({element: "div", pseudoElement: "first-letter", property: "color", feather: true}).property);
//console.dir(ExtractCSS({element: "div", class: "ftest", pseudoElement: "first-letter", property: "color", feather: true})[0].property);
//console.timeEnd('Feather');
