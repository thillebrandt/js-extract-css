Array.prototype.extendMap = function(callback, thisArg) {
	let T, k;

	if (this == null) {
		throw new TypeError(' this is null or not defined');
	}

	const O = Object(this);
	const len = O.length >>> 0;

	if (typeof callback !== 'function') {
		throw new TypeError(callback + ' is not a function');
	}

	if (arguments.length > 1) {
		T = thisArg;
	}

	const A = new Array(len);
	k = 0;

	while (k < this.length) {
		let kValue, mappedValue;

		if (k in O) {
			kValue = O[k];
			mappedValue = callback.call(T, kValue, k, O);
			A[k] = mappedValue;
		}

		k++;
	}

	return A;
};


//export default function ExtractCSS(inputs) {
export default function ExtractCSS(inputs, callback) {
/*	inputs = Object.assign({
		element: undefined,
		class: undefined,
		id: undefined,
		property: undefined,
		feather: false
	}, inputs);
*/
	try {
		const sheets = Array.from(document.styleSheets);
		const animations = {};
		const results = [];
		const weightedResults = {};

		sheets.map((sheet) => {
			if (!sheet.cssRules) throw new ExtractError(405, "No CSS Rules!\nIn certain browsers, CSS rules cannot be accessed if they are loaded from local disk or cross origin.");
			const cssSheet = Array.from(sheet.cssRules);
			cssSheet.extendMap((rule) => {
				const parseText = rule.selectorText;

				if (rule.type === 4) {
					// @media
					cssSheet.push(...Array.from(rule.cssRules));
				}

				if (rule.type === 7) {
					// @keyframes
					animations[rule.name] = rule.cssRules;
				}

				if (parseText) {
					const selectors = rule.selectorText.split(' ');
					selectors.map((selector) => {
						const searchObject = ParseCSS(selector);
						searchObject.styles = {};

						if (rule.parentRule) {
							// If @media add conditions to object
							if (searchObject.conditions) {
								searchObject.conditions.push(rule.parentRule.conditionText.split(/(?:and)|\s|\((.+)\)/).filter((a) => { if (a) return a; }));
							} else {
								searchObject.conditions = rule.parentRule.conditionText.split(/(?:and)|\s|\((.+)\)/).filter((a) => { if (a) return a; });
							}
						}

						let aMatch = undefined;
						Object.keys(inputs).map((key) => {
							if (["element", "class", "id", "pseudoelement", "pseudoclass"].includes(key.toLowerCase()) && (aMatch === undefined || aMatch)) {
								if (inputs[key] === searchObject[key] || (inputs[key] === "" && !searchObject[key])) aMatch = true;
								else aMatch = false;
							}
						});

						let weight = 0;
						if (inputs.feather && !aMatch) {
							let multi = 0;

							(inputs.element === searchObject.element && inputs.element != "" && !searchObject.class) ? weight += 1 : null;
							(inputs.class && inputs.class === searchObject.class) ? multi += 1 : null;
							(inputs.id && inputs.id === searchObject.id) ? multi += 1 : null;

							if ((inputs.class && inputs.class === searchObject.class) || (inputs.id && inputs.id === searchObject.id)) {
								weight += 3 * multi;
								(inputs.element === searchObject.element) ? weight += 6 * multi : null;
								if (inputs.element != searchObject.element) {
									(searchObject.element === "" && inputs.element != "") ? weight += 2 * multi : null;
									(searchObject.element != "" && inputs.element === "") ? weight += 1 * multi : null;
								}

								if (inputs.pseudoElement) {
									(inputs.pseudoElement === searchObject.pseudoElement) ? weight += (2 * multi) + 1 : null;
									(!searchObject.pseudoElement) ? weight += 1 * multi : null;
								}

								if (inputs.pseudoClass) {
									(inputs.pseudoClass === searchObject.pseudoClass) ? weight += (2 * multi) + 1 : null;
									(!searchObject.pseudoClass) ? weight += 1 * multi : null;
								}
							}
						}

						if (aMatch || weight > 0) {
							Array.from(rule.style).map((style) => {
								// Gather registered styles
								if (style && style.toString().split('-')[0] != "") {
									if (["animation", "border", "margin", "padding", "background"].includes(style.toString().split('-')[0])) {
										searchObject.styles[style.toString().split('-')[0]] = rule.style[style.toString().split('-')[0]];
										if (style.toString() === "animation-name") {
											// For animations, gather the registered @keyframes
											if (animations[rule.style[style.toString()]]) {
												searchObject.styles.animationFrames = BreakdownAnimation(animations[rule.style[style.toString()]]);
											} else {
												if (inputs.property && inputs.property === "animation") {
													throw new ExtractError(404, "Animation has no frames");
												} else {
													searchObject.styles.animationFrames = {404: "Animation has no frames"};
												}
											}
										}
									} else {
										searchObject.styles[style.toString()] = rule.style[style.toString()];
									}
								}
							});

							if (inputs.property) {
								if (!searchObject.styles[inputs.property.toLowerCase()]) {
									aMatch = false;
									weight = 0;
								} else {
									searchObject.property = {};
									searchObject.property[inputs.property.toLowerCase()] = searchObject.styles[inputs.property.toLowerCase()];
									if (inputs.property.toLowerCase() === "animation") {
										searchObject.property.animationFrames = searchObject.styles.animationFrames;
									}
								}
							}
						}

						if (aMatch) {
							results.push(searchObject);
						} else if (weight > 0) {
							weightedResults[weight] = searchObject;
						}
					});
				}
			});
		});

		if (results.length === 0) {
			const weightedList = Object.keys(weightedResults);
			weightedList.sort(function(a, b) {return b - a;});
			const weightedFinal = [];
			weightedList.map((weight) => {
				weightedFinal.push(weightedResults[weight]);
			});
			if (weightedFinal.length === 0 || !inputs.feather) {
				callback(undefined);
//				return undefined;
			} else {
				if (weightedFinal.length === 1 || inputs.firstOnly) {
					callback(weightedFinal[0]);
//					return weightedFinal[0];
				} else {
					callback(weightedFinal);
//					return weightedFinal;
				}
			}
		} else if (results.length === 1 || inputs.firstOnly) {
			callback(results[0]);
//			return results[0];
		} else {
			callback(results);
//			return results;
		}

	}
	catch(e) {
//		console.error(`ERROR! ${e.status}: ${e.message}`);
		callback({status: e.status, message: e.message});
//		return {status: e.status, message: e.message};
	}
}

function ParseCSS(input) {
	const css = input.split(/(::|\[.+\]|,|:|\s|\.|#)/);
	const searchObject = {};

	if (css) {
		css.map((segment, index) => {
			let attrib = [];
			switch (index % 2) {
			case 0:
				switch (index) {
				case 0:
					// First segment, if not empty, will always be the element.
					searchObject.element = segment;
					break;
				default:
					// Even segments, that are not first, can be a variety of things;
					// class, id, pseudo-class, pseudo-element
					switch (css[index - 1]) {
					case ",":
						// standard separator; not counted.
						break;
					case ".":
						// class
						searchObject.class = (searchObject.class === undefined) ? segment : searchObject.class;
						break;
					case "#":
						// id
						searchObject.id = (searchObject.id === undefined) ? segment : searchObject.id;
						break;
					case ":":
						// pseudo-class
						searchObject.pseudoClass = (searchObject.pseudoClass === undefined) ? segment : searchObject.pseudoClass;
						break;
					case "::":
						// pseudo-element
						searchObject.pseudoElement = (searchObject.pseudoElement === undefined) ? segment : searchObject.pseudoElement;
						break;
					default:
						attrib = css[index - 1].split(/([\[\]])/);
						if (attrib[1] === "[" && attrib[3] === "]") {
							searchObject.attributeSelector = (searchObject.attributeSelector === undefined) ? attrib[2] : searchObject.attributeSelector;
						}
					}
					break;
				}
				break;
			}
		});
	}

	return searchObject;
}

function BreakdownAnimation(animationObject) {
	const animationSet = {};
	const animationList = Array.from(animationObject);

	animationList.map((keyFrame) => {
		const keyText = keyFrame.keyText;
		animationSet[keyText] = {};
		const keyStyles = Array.from(keyFrame.style);
		keyStyles.map((style) => {
			if (style) {
				animationSet[keyText][style.toString()] = keyFrame.style[style.toString()];
			}
		});
	});

	return animationSet;
}


function ExtractError(code, message) {
	this.status = code;
	this.message = message;
	this.toString = function() {
		return this;
	};
}