(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

exports.default = ExtractCSS;

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

Array.prototype.extendMap = function (callback, thisArg) {
	var T = void 0,
	    k = void 0;

	if (this == null) {
		throw new TypeError(' this is null or not defined');
	}

	var O = Object(this);
	var len = O.length >>> 0;

	if (typeof callback !== 'function') {
		throw new TypeError(callback + ' is not a function');
	}

	if (arguments.length > 1) {
		T = thisArg;
	}

	var A = new Array(len);
	k = 0;

	while (k < this.length) {
		var kValue = void 0,
		    mappedValue = void 0;

		if (k in O) {
			kValue = O[k];
			mappedValue = callback.call(T, kValue, k, O);
			A[k] = mappedValue;
		}

		k++;
	}

	return A;
};

function ExtractCSS(inputs) {
	/*	inputs = Object.assign({
 		element: undefined,
 		class: undefined,
 		id: undefined,
 		property: undefined,
 		feather: false
 	}, inputs);
 */

	try {
		var _ret = function () {
			var sheets = Array.from(document.styleSheets);
			var animations = {};
			var results = [];
			var weightedResults = {};

			sheets.map(function (sheet) {
				if (!sheet.cssRules) throw new ExtractError(405, "No CSS Rules!\nIn certain browsers, CSS rules cannot be accessed if they are loaded from local disk or cross origin.");
				var cssSheet = Array.from(sheet.cssRules);
				cssSheet.extendMap(function (rule) {
					var parseText = rule.selectorText;

					if (rule.type === 4) {
						// @media
						cssSheet.push.apply(cssSheet, _toConsumableArray(Array.from(rule.cssRules)));
					}

					if (rule.type === 7) {
						// @keyframes
						animations[rule.name] = rule.cssRules;
					}

					if (parseText) {
						var selectors = rule.selectorText.split(' ');
						selectors.map(function (selector) {
							var searchObject = ParseCSS(selector);
							searchObject.styles = {};

							if (rule.parentRule) {
								// If @media add conditions to object
								if (searchObject.conditions) {
									searchObject.conditions.push(rule.parentRule.conditionText.split(/(?:and)|\s|\((.+)\)/).filter(function (a) {
										if (a) return a;
									}));
								} else {
									searchObject.conditions = rule.parentRule.conditionText.split(/(?:and)|\s|\((.+)\)/).filter(function (a) {
										if (a) return a;
									});
								}
							}

							var aMatch = undefined;
							Object.keys(inputs).map(function (key) {
								if (["element", "class", "id", "pseudoelement", "pseudoclass"].includes(key.toLowerCase()) && (aMatch === undefined || aMatch)) {
									if (inputs[key] === searchObject[key] || inputs[key] === "" && !searchObject[key]) aMatch = true;else aMatch = false;
								}
							});

							var weight = 0;
							if (inputs.feather && !aMatch) {
								var multi = 0;

								inputs.element === searchObject.element && inputs.element != "" && !searchObject.class ? weight += 1 : null;
								inputs.class && inputs.class === searchObject.class ? multi += 1 : null;
								inputs.id && inputs.id === searchObject.id ? multi += 1 : null;

								if (inputs.class && inputs.class === searchObject.class || inputs.id && inputs.id === searchObject.id) {
									weight += 3 * multi;
									inputs.element === searchObject.element ? weight += 6 * multi : null;
									if (inputs.element != searchObject.element) {
										searchObject.element === "" && inputs.element != "" ? weight += 2 * multi : null;
										searchObject.element != "" && inputs.element === "" ? weight += 1 * multi : null;
									}

									if (inputs.pseudoElement) {
										inputs.pseudoElement === searchObject.pseudoElement ? weight += 2 * multi + 1 : null;
										!searchObject.pseudoElement ? weight += 1 * multi : null;
									}

									if (inputs.pseudoClass) {
										inputs.pseudoClass === searchObject.pseudoClass ? weight += 2 * multi + 1 : null;
										!searchObject.pseudoClass ? weight += 1 * multi : null;
									}
								}
							}

							if (aMatch || weight > 0) {
								Array.from(rule.style).map(function (style) {
									// Gather registered styles
									if (style && style.toString().split('-')[0] != "") {
										if (["animation", "border", "margin", "padding", "background"].includes(style.toString().split('-')[0])) {
											searchObject.styles[style.toString().split('-')[0]] = rule.style[style.toString().split('-')[0]];
											if (style.toString() === "animation-name") {
												// For animations, gather the registered @keyframes
												if (animations[rule.style[style.toString()]]) {
													searchObject.styles.animationFrames = BreakdownAnimation(animations[rule.style[style.toString()]]);
												} else {
													if (inputs.property && inputs.property === "animation") {
														throw new ExtractError(404, "Animation has no frames");
													} else {
														searchObject.styles.animationFrames = { 404: "Animation has no frames" };
													}
												}
											}
										} else {
											searchObject.styles[style.toString()] = rule.style[style.toString()];
										}
									}
								});

								if (inputs.property) {
									if (!searchObject.styles[inputs.property.toLowerCase()]) {
										aMatch = false;
										weight = 0;
									} else {
										searchObject.property = {};
										searchObject.property[inputs.property.toLowerCase()] = searchObject.styles[inputs.property.toLowerCase()];
										if (inputs.property.toLowerCase() === "animation") {
											searchObject.property.animationFrames = searchObject.styles.animationFrames;
										}
									}
								}
							}

							if (aMatch) {
								results.push(searchObject);
							} else if (weight > 0) {
								weightedResults[weight] = searchObject;
							}
						});
					}
				});
			});

			if (results.length === 0) {
				var _ret2 = function () {
					var weightedList = Object.keys(weightedResults);
					weightedList.sort(function (a, b) {
						return b - a;
					});
					var weightedFinal = [];
					weightedList.map(function (weight) {
						weightedFinal.push(weightedResults[weight]);
					});
					if (weightedFinal.length === 0 || !inputs.feather) {
						return {
							v: {
								v: undefined
							}
						};
					} else {
						if (weightedFinal.length === 1 || inputs.firstOnly) {
							return {
								v: {
									v: weightedFinal[0]
								}
							};
						} else {
							return {
								v: {
									v: weightedFinal
								}
							};
						}
					}
				}();

				if ((typeof _ret2 === 'undefined' ? 'undefined' : _typeof(_ret2)) === "object") return _ret2.v;
			} else if (results.length === 1 || inputs.firstOnly) {
				return {
					v: results[0]
				};
			} else {
				return {
					v: results
				};
			}
		}();

		if ((typeof _ret === 'undefined' ? 'undefined' : _typeof(_ret)) === "object") return _ret.v;
	} catch (e) {
		console.error(e.status + ': ' + e.message);
		return { status: e.status, message: e.message };
	}
}

function ParseCSS(input) {
	var css = input.split(/(::|\[.+\]|,|:|\s|\.|#)/);
	var searchObject = {};

	if (css) {
		css.map(function (segment, index) {
			var attrib = [];
			switch (index % 2) {
				case 0:
					switch (index) {
						case 0:
							// First segment, if not empty, will always be the element.
							searchObject.element = segment;
							break;
						default:
							// Even segments, that are not first, can be a variety of things;
							// class, id, pseudo-class, pseudo-element
							switch (css[index - 1]) {
								case ",":
									// standard separator; not counted.
									break;
								case ".":
									// class
									searchObject.class = searchObject.class === undefined ? segment : searchObject.class;
									break;
								case "#":
									// id
									searchObject.id = searchObject.id === undefined ? segment : searchObject.id;
									break;
								case ":":
									// pseudo-class
									searchObject.pseudoClass = searchObject.pseudoClass === undefined ? segment : searchObject.pseudoClass;
									break;
								case "::":
									// pseudo-element
									searchObject.pseudoElement = searchObject.pseudoElement === undefined ? segment : searchObject.pseudoElement;
									break;
								default:
									attrib = css[index - 1].split(/([\[\]])/);
									if (attrib[1] === "[" && attrib[3] === "]") {
										searchObject.attributeSelector = searchObject.attributeSelector === undefined ? attrib[2] : searchObject.attributeSelector;
									}
							}
							break;
					}
					break;
			}
		});
	}

	return searchObject;
}

function BreakdownAnimation(animationObject) {
	var animationSet = {};
	var animationList = Array.from(animationObject);

	animationList.map(function (keyFrame) {
		var keyText = keyFrame.keyText;
		animationSet[keyText] = {};
		var keyStyles = Array.from(keyFrame.style);
		keyStyles.map(function (style) {
			if (style) {
				animationSet[keyText][style.toString()] = keyFrame.style[style.toString()];
			}
		});
	});

	return animationSet;
}

function ExtractError(code, message) {
	this.status = code;
	this.message = message;
	this.toString = function () {
		return this;
	};
}

},{}],2:[function(require,module,exports){
'use strict';

var _extractCss = require('./src/extract-css');

var _extractCss2 = _interopRequireDefault(_extractCss);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

console.time('Extract');
console.dir((0, _extractCss2.default)({ class: "roll", property: "animation" }).property);
console.dir((0, _extractCss2.default)({ element: "p", class: "pklasse", property: "color" }));
console.dir((0, _extractCss2.default)({ element: "h2" }));
console.dir((0, _extractCss2.default)({ class: "ryll", property: "width" }));
console.dir((0, _extractCss2.default)({ class: "pklasse", property: "color" }));
console.dir((0, _extractCss2.default)({ element: "", class: "pklasse", property: "color" }));
console.dir((0, _extractCss2.default)({ class: "pklasse", property: "color", firstOnly: true }));
console.timeEnd('Extract');

//console.time('Feather');
//console.dir(ExtractCSS({element: "div", pseudoElement: "first-letter", property: "color", feather: true}).property);
//console.dir(ExtractCSS({element: "div", class: "ftest", pseudoElement: "first-letter", property: "color", feather: true})[0].property);
//console.timeEnd('Feather');

},{"./src/extract-css":1}]},{},[2])
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyaWZ5L25vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJzcmNcXGV4dHJhY3QtY3NzLmpzIiwidGVzdC5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTs7Ozs7Ozs7O2tCQ3FDd0IsVTs7OztBQXJDeEIsTUFBTSxTQUFOLENBQWdCLFNBQWhCLEdBQTRCLFVBQVMsUUFBVCxFQUFtQixPQUFuQixFQUE0QjtBQUN2RCxLQUFJLFVBQUo7QUFBQSxLQUFPLFVBQVA7O0FBRUEsS0FBSSxRQUFRLElBQVosRUFBa0I7QUFDakIsUUFBTSxJQUFJLFNBQUosQ0FBYyw4QkFBZCxDQUFOO0FBQ0E7O0FBRUQsS0FBTSxJQUFJLE9BQU8sSUFBUCxDQUFWO0FBQ0EsS0FBTSxNQUFNLEVBQUUsTUFBRixLQUFhLENBQXpCOztBQUVBLEtBQUksT0FBTyxRQUFQLEtBQW9CLFVBQXhCLEVBQW9DO0FBQ25DLFFBQU0sSUFBSSxTQUFKLENBQWMsV0FBVyxvQkFBekIsQ0FBTjtBQUNBOztBQUVELEtBQUksVUFBVSxNQUFWLEdBQW1CLENBQXZCLEVBQTBCO0FBQ3pCLE1BQUksT0FBSjtBQUNBOztBQUVELEtBQU0sSUFBSSxJQUFJLEtBQUosQ0FBVSxHQUFWLENBQVY7QUFDQSxLQUFJLENBQUo7O0FBRUEsUUFBTyxJQUFJLEtBQUssTUFBaEIsRUFBd0I7QUFDdkIsTUFBSSxlQUFKO0FBQUEsTUFBWSxvQkFBWjs7QUFFQSxNQUFJLEtBQUssQ0FBVCxFQUFZO0FBQ1gsWUFBUyxFQUFFLENBQUYsQ0FBVDtBQUNBLGlCQUFjLFNBQVMsSUFBVCxDQUFjLENBQWQsRUFBaUIsTUFBakIsRUFBeUIsQ0FBekIsRUFBNEIsQ0FBNUIsQ0FBZDtBQUNBLEtBQUUsQ0FBRixJQUFPLFdBQVA7QUFDQTs7QUFFRDtBQUNBOztBQUVELFFBQU8sQ0FBUDtBQUNBLENBbENEOztBQXFDZSxTQUFTLFVBQVQsQ0FBb0IsTUFBcEIsRUFBNEI7QUFDM0M7Ozs7Ozs7OztBQVNDLEtBQUk7QUFBQTtBQUNILE9BQU0sU0FBUyxNQUFNLElBQU4sQ0FBVyxTQUFTLFdBQXBCLENBQWY7QUFDQSxPQUFNLGFBQWEsRUFBbkI7QUFDQSxPQUFNLFVBQVUsRUFBaEI7QUFDQSxPQUFNLGtCQUFrQixFQUF4Qjs7QUFFQSxVQUFPLEdBQVAsQ0FBVyxVQUFDLEtBQUQsRUFBVztBQUNyQixRQUFJLENBQUMsTUFBTSxRQUFYLEVBQXFCLE1BQU0sSUFBSSxZQUFKLENBQWlCLEdBQWpCLEVBQXNCLHNIQUF0QixDQUFOO0FBQ3JCLFFBQU0sV0FBVyxNQUFNLElBQU4sQ0FBVyxNQUFNLFFBQWpCLENBQWpCO0FBQ0EsYUFBUyxTQUFULENBQW1CLFVBQUMsSUFBRCxFQUFVO0FBQzVCLFNBQU0sWUFBWSxLQUFLLFlBQXZCOztBQUVBLFNBQUksS0FBSyxJQUFMLEtBQWMsQ0FBbEIsRUFBcUI7QUFDcEI7QUFDQSxlQUFTLElBQVQsb0NBQWlCLE1BQU0sSUFBTixDQUFXLEtBQUssUUFBaEIsQ0FBakI7QUFDQTs7QUFFRCxTQUFJLEtBQUssSUFBTCxLQUFjLENBQWxCLEVBQXFCO0FBQ3BCO0FBQ0EsaUJBQVcsS0FBSyxJQUFoQixJQUF3QixLQUFLLFFBQTdCO0FBQ0E7O0FBRUQsU0FBSSxTQUFKLEVBQWU7QUFDZCxVQUFNLFlBQVksS0FBSyxZQUFMLENBQWtCLEtBQWxCLENBQXdCLEdBQXhCLENBQWxCO0FBQ0EsZ0JBQVUsR0FBVixDQUFjLFVBQUMsUUFBRCxFQUFjO0FBQzNCLFdBQU0sZUFBZSxTQUFTLFFBQVQsQ0FBckI7QUFDQSxvQkFBYSxNQUFiLEdBQXNCLEVBQXRCOztBQUVBLFdBQUksS0FBSyxVQUFULEVBQXFCO0FBQ3BCO0FBQ0EsWUFBSSxhQUFhLFVBQWpCLEVBQTZCO0FBQzVCLHNCQUFhLFVBQWIsQ0FBd0IsSUFBeEIsQ0FBNkIsS0FBSyxVQUFMLENBQWdCLGFBQWhCLENBQThCLEtBQTlCLENBQW9DLHFCQUFwQyxFQUEyRCxNQUEzRCxDQUFrRSxVQUFDLENBQUQsRUFBTztBQUFFLGNBQUksQ0FBSixFQUFPLE9BQU8sQ0FBUDtBQUFXLFVBQTdGLENBQTdCO0FBQ0EsU0FGRCxNQUVPO0FBQ04sc0JBQWEsVUFBYixHQUEwQixLQUFLLFVBQUwsQ0FBZ0IsYUFBaEIsQ0FBOEIsS0FBOUIsQ0FBb0MscUJBQXBDLEVBQTJELE1BQTNELENBQWtFLFVBQUMsQ0FBRCxFQUFPO0FBQUUsY0FBSSxDQUFKLEVBQU8sT0FBTyxDQUFQO0FBQVcsVUFBN0YsQ0FBMUI7QUFDQTtBQUNEOztBQUVELFdBQUksU0FBUyxTQUFiO0FBQ0EsY0FBTyxJQUFQLENBQVksTUFBWixFQUFvQixHQUFwQixDQUF3QixVQUFDLEdBQUQsRUFBUztBQUNoQyxZQUFJLENBQUMsU0FBRCxFQUFZLE9BQVosRUFBcUIsSUFBckIsRUFBMkIsZUFBM0IsRUFBNEMsYUFBNUMsRUFBMkQsUUFBM0QsQ0FBb0UsSUFBSSxXQUFKLEVBQXBFLE1BQTJGLFdBQVcsU0FBWCxJQUF3QixNQUFuSCxDQUFKLEVBQWdJO0FBQy9ILGFBQUksT0FBTyxHQUFQLE1BQWdCLGFBQWEsR0FBYixDQUFoQixJQUFzQyxPQUFPLEdBQVAsTUFBZ0IsRUFBaEIsSUFBc0IsQ0FBQyxhQUFhLEdBQWIsQ0FBakUsRUFBcUYsU0FBUyxJQUFULENBQXJGLEtBQ0ssU0FBUyxLQUFUO0FBQ0w7QUFDRCxRQUxEOztBQU9BLFdBQUksU0FBUyxDQUFiO0FBQ0EsV0FBSSxPQUFPLE9BQVAsSUFBa0IsQ0FBQyxNQUF2QixFQUErQjtBQUM5QixZQUFJLFFBQVEsQ0FBWjs7QUFFQyxlQUFPLE9BQVAsS0FBbUIsYUFBYSxPQUFoQyxJQUEyQyxPQUFPLE9BQVAsSUFBa0IsRUFBN0QsSUFBbUUsQ0FBQyxhQUFhLEtBQWxGLEdBQTJGLFVBQVUsQ0FBckcsR0FBeUcsSUFBekc7QUFDQyxlQUFPLEtBQVAsSUFBZ0IsT0FBTyxLQUFQLEtBQWlCLGFBQWEsS0FBL0MsR0FBd0QsU0FBUyxDQUFqRSxHQUFxRSxJQUFyRTtBQUNDLGVBQU8sRUFBUCxJQUFhLE9BQU8sRUFBUCxLQUFjLGFBQWEsRUFBekMsR0FBK0MsU0FBUyxDQUF4RCxHQUE0RCxJQUE1RDs7QUFFQSxZQUFLLE9BQU8sS0FBUCxJQUFnQixPQUFPLEtBQVAsS0FBaUIsYUFBYSxLQUEvQyxJQUEwRCxPQUFPLEVBQVAsSUFBYSxPQUFPLEVBQVAsS0FBYyxhQUFhLEVBQXRHLEVBQTJHO0FBQzFHLG1CQUFVLElBQUksS0FBZDtBQUNDLGdCQUFPLE9BQVAsS0FBbUIsYUFBYSxPQUFqQyxHQUE0QyxVQUFVLElBQUksS0FBMUQsR0FBa0UsSUFBbEU7QUFDQSxhQUFJLE9BQU8sT0FBUCxJQUFrQixhQUFhLE9BQW5DLEVBQTRDO0FBQzFDLHVCQUFhLE9BQWIsS0FBeUIsRUFBekIsSUFBK0IsT0FBTyxPQUFQLElBQWtCLEVBQWxELEdBQXdELFVBQVUsSUFBSSxLQUF0RSxHQUE4RSxJQUE5RTtBQUNDLHVCQUFhLE9BQWIsSUFBd0IsRUFBeEIsSUFBOEIsT0FBTyxPQUFQLEtBQW1CLEVBQWxELEdBQXdELFVBQVUsSUFBSSxLQUF0RSxHQUE4RSxJQUE5RTtBQUNBOztBQUVELGFBQUksT0FBTyxhQUFYLEVBQTBCO0FBQ3hCLGlCQUFPLGFBQVAsS0FBeUIsYUFBYSxhQUF2QyxHQUF3RCxVQUFXLElBQUksS0FBTCxHQUFjLENBQWhGLEdBQW9GLElBQXBGO0FBQ0MsV0FBQyxhQUFhLGFBQWYsR0FBZ0MsVUFBVSxJQUFJLEtBQTlDLEdBQXNELElBQXREO0FBQ0E7O0FBRUQsYUFBSSxPQUFPLFdBQVgsRUFBd0I7QUFDdEIsaUJBQU8sV0FBUCxLQUF1QixhQUFhLFdBQXJDLEdBQW9ELFVBQVcsSUFBSSxLQUFMLEdBQWMsQ0FBNUUsR0FBZ0YsSUFBaEY7QUFDQyxXQUFDLGFBQWEsV0FBZixHQUE4QixVQUFVLElBQUksS0FBNUMsR0FBb0QsSUFBcEQ7QUFDQTtBQUNEO0FBQ0Q7O0FBRUQsV0FBSSxVQUFVLFNBQVMsQ0FBdkIsRUFBMEI7QUFDekIsY0FBTSxJQUFOLENBQVcsS0FBSyxLQUFoQixFQUF1QixHQUF2QixDQUEyQixVQUFDLEtBQUQsRUFBVztBQUNyQztBQUNBLGFBQUksU0FBUyxNQUFNLFFBQU4sR0FBaUIsS0FBakIsQ0FBdUIsR0FBdkIsRUFBNEIsQ0FBNUIsS0FBa0MsRUFBL0MsRUFBbUQ7QUFDbEQsY0FBSSxDQUFDLFdBQUQsRUFBYyxRQUFkLEVBQXdCLFFBQXhCLEVBQWtDLFNBQWxDLEVBQTZDLFlBQTdDLEVBQTJELFFBQTNELENBQW9FLE1BQU0sUUFBTixHQUFpQixLQUFqQixDQUF1QixHQUF2QixFQUE0QixDQUE1QixDQUFwRSxDQUFKLEVBQXlHO0FBQ3hHLHdCQUFhLE1BQWIsQ0FBb0IsTUFBTSxRQUFOLEdBQWlCLEtBQWpCLENBQXVCLEdBQXZCLEVBQTRCLENBQTVCLENBQXBCLElBQXNELEtBQUssS0FBTCxDQUFXLE1BQU0sUUFBTixHQUFpQixLQUFqQixDQUF1QixHQUF2QixFQUE0QixDQUE1QixDQUFYLENBQXREO0FBQ0EsZUFBSSxNQUFNLFFBQU4sT0FBcUIsZ0JBQXpCLEVBQTJDO0FBQzFDO0FBQ0EsZ0JBQUksV0FBVyxLQUFLLEtBQUwsQ0FBVyxNQUFNLFFBQU4sRUFBWCxDQUFYLENBQUosRUFBOEM7QUFDN0MsMEJBQWEsTUFBYixDQUFvQixlQUFwQixHQUFzQyxtQkFBbUIsV0FBVyxLQUFLLEtBQUwsQ0FBVyxNQUFNLFFBQU4sRUFBWCxDQUFYLENBQW5CLENBQXRDO0FBQ0EsYUFGRCxNQUVPO0FBQ04saUJBQUksT0FBTyxRQUFQLElBQW1CLE9BQU8sUUFBUCxLQUFvQixXQUEzQyxFQUF3RDtBQUN2RCxvQkFBTSxJQUFJLFlBQUosQ0FBaUIsR0FBakIsRUFBc0IseUJBQXRCLENBQU47QUFDQSxjQUZELE1BRU87QUFDTiwyQkFBYSxNQUFiLENBQW9CLGVBQXBCLEdBQXNDLEVBQUMsS0FBSyx5QkFBTixFQUF0QztBQUNBO0FBQ0Q7QUFDRDtBQUNELFdBZEQsTUFjTztBQUNOLHdCQUFhLE1BQWIsQ0FBb0IsTUFBTSxRQUFOLEVBQXBCLElBQXdDLEtBQUssS0FBTCxDQUFXLE1BQU0sUUFBTixFQUFYLENBQXhDO0FBQ0E7QUFDRDtBQUNELFNBckJEOztBQXVCQSxZQUFJLE9BQU8sUUFBWCxFQUFxQjtBQUNwQixhQUFJLENBQUMsYUFBYSxNQUFiLENBQW9CLE9BQU8sUUFBUCxDQUFnQixXQUFoQixFQUFwQixDQUFMLEVBQXlEO0FBQ3hELG1CQUFTLEtBQVQ7QUFDQSxtQkFBUyxDQUFUO0FBQ0EsVUFIRCxNQUdPO0FBQ04sdUJBQWEsUUFBYixHQUF3QixFQUF4QjtBQUNBLHVCQUFhLFFBQWIsQ0FBc0IsT0FBTyxRQUFQLENBQWdCLFdBQWhCLEVBQXRCLElBQXVELGFBQWEsTUFBYixDQUFvQixPQUFPLFFBQVAsQ0FBZ0IsV0FBaEIsRUFBcEIsQ0FBdkQ7QUFDQSxjQUFJLE9BQU8sUUFBUCxDQUFnQixXQUFoQixPQUFrQyxXQUF0QyxFQUFtRDtBQUNsRCx3QkFBYSxRQUFiLENBQXNCLGVBQXRCLEdBQXdDLGFBQWEsTUFBYixDQUFvQixlQUE1RDtBQUNBO0FBQ0Q7QUFDRDtBQUNEOztBQUVELFdBQUksTUFBSixFQUFZO0FBQ1gsZ0JBQVEsSUFBUixDQUFhLFlBQWI7QUFDQSxRQUZELE1BRU8sSUFBSSxTQUFTLENBQWIsRUFBZ0I7QUFDdEIsd0JBQWdCLE1BQWhCLElBQTBCLFlBQTFCO0FBQ0E7QUFDRCxPQTVGRDtBQTZGQTtBQUNELEtBN0dEO0FBOEdBLElBakhEOztBQW1IQSxPQUFJLFFBQVEsTUFBUixLQUFtQixDQUF2QixFQUEwQjtBQUFBO0FBQ3pCLFNBQU0sZUFBZSxPQUFPLElBQVAsQ0FBWSxlQUFaLENBQXJCO0FBQ0Esa0JBQWEsSUFBYixDQUFrQixVQUFTLENBQVQsRUFBWSxDQUFaLEVBQWU7QUFBQyxhQUFPLElBQUksQ0FBWDtBQUFjLE1BQWhEO0FBQ0EsU0FBTSxnQkFBZ0IsRUFBdEI7QUFDQSxrQkFBYSxHQUFiLENBQWlCLFVBQUMsTUFBRCxFQUFZO0FBQzVCLG9CQUFjLElBQWQsQ0FBbUIsZ0JBQWdCLE1BQWhCLENBQW5CO0FBQ0EsTUFGRDtBQUdBLFNBQUksY0FBYyxNQUFkLEtBQXlCLENBQXpCLElBQThCLENBQUMsT0FBTyxPQUExQyxFQUFtRDtBQUNsRDtBQUFBO0FBQUEsV0FBTztBQUFQO0FBQUE7QUFDQSxNQUZELE1BRU87QUFDTixVQUFJLGNBQWMsTUFBZCxLQUF5QixDQUF6QixJQUE4QixPQUFPLFNBQXpDLEVBQW9EO0FBQ25EO0FBQUE7QUFBQSxZQUFPLGNBQWMsQ0FBZDtBQUFQO0FBQUE7QUFDQSxPQUZELE1BRU87QUFDTjtBQUFBO0FBQUEsWUFBTztBQUFQO0FBQUE7QUFDQTtBQUNEO0FBZndCOztBQUFBO0FBZ0J6QixJQWhCRCxNQWdCTyxJQUFJLFFBQVEsTUFBUixLQUFtQixDQUFuQixJQUF3QixPQUFPLFNBQW5DLEVBQThDO0FBQ3BEO0FBQUEsUUFBTyxRQUFRLENBQVI7QUFBUDtBQUNBLElBRk0sTUFFQTtBQUNOO0FBQUEsUUFBTztBQUFQO0FBQ0E7QUE3SUU7O0FBQUE7QUErSUgsRUEvSUQsQ0FnSkEsT0FBTSxDQUFOLEVBQVM7QUFDUixVQUFRLEtBQVIsQ0FBaUIsRUFBRSxNQUFuQixVQUE4QixFQUFFLE9BQWhDO0FBQ0EsU0FBTyxFQUFDLFFBQVEsRUFBRSxNQUFYLEVBQW1CLFNBQVMsRUFBRSxPQUE5QixFQUFQO0FBQ0E7QUFDRDs7QUFFRCxTQUFTLFFBQVQsQ0FBa0IsS0FBbEIsRUFBeUI7QUFDeEIsS0FBTSxNQUFNLE1BQU0sS0FBTixDQUFZLHlCQUFaLENBQVo7QUFDQSxLQUFNLGVBQWUsRUFBckI7O0FBRUEsS0FBSSxHQUFKLEVBQVM7QUFDUixNQUFJLEdBQUosQ0FBUSxVQUFDLE9BQUQsRUFBVSxLQUFWLEVBQW9CO0FBQzNCLE9BQUksU0FBUyxFQUFiO0FBQ0EsV0FBUSxRQUFRLENBQWhCO0FBQ0EsU0FBSyxDQUFMO0FBQ0MsYUFBUSxLQUFSO0FBQ0EsV0FBSyxDQUFMO0FBQ0M7QUFDQSxvQkFBYSxPQUFiLEdBQXVCLE9BQXZCO0FBQ0E7QUFDRDtBQUNDO0FBQ0E7QUFDQSxlQUFRLElBQUksUUFBUSxDQUFaLENBQVI7QUFDQSxhQUFLLEdBQUw7QUFDQztBQUNBO0FBQ0QsYUFBSyxHQUFMO0FBQ0M7QUFDQSxzQkFBYSxLQUFiLEdBQXNCLGFBQWEsS0FBYixLQUF1QixTQUF4QixHQUFxQyxPQUFyQyxHQUErQyxhQUFhLEtBQWpGO0FBQ0E7QUFDRCxhQUFLLEdBQUw7QUFDQztBQUNBLHNCQUFhLEVBQWIsR0FBbUIsYUFBYSxFQUFiLEtBQW9CLFNBQXJCLEdBQWtDLE9BQWxDLEdBQTRDLGFBQWEsRUFBM0U7QUFDQTtBQUNELGFBQUssR0FBTDtBQUNDO0FBQ0Esc0JBQWEsV0FBYixHQUE0QixhQUFhLFdBQWIsS0FBNkIsU0FBOUIsR0FBMkMsT0FBM0MsR0FBcUQsYUFBYSxXQUE3RjtBQUNBO0FBQ0QsYUFBSyxJQUFMO0FBQ0M7QUFDQSxzQkFBYSxhQUFiLEdBQThCLGFBQWEsYUFBYixLQUErQixTQUFoQyxHQUE2QyxPQUE3QyxHQUF1RCxhQUFhLGFBQWpHO0FBQ0E7QUFDRDtBQUNDLGtCQUFTLElBQUksUUFBUSxDQUFaLEVBQWUsS0FBZixDQUFxQixVQUFyQixDQUFUO0FBQ0EsYUFBSSxPQUFPLENBQVAsTUFBYyxHQUFkLElBQXFCLE9BQU8sQ0FBUCxNQUFjLEdBQXZDLEVBQTRDO0FBQzNDLHVCQUFhLGlCQUFiLEdBQWtDLGFBQWEsaUJBQWIsS0FBbUMsU0FBcEMsR0FBaUQsT0FBTyxDQUFQLENBQWpELEdBQTZELGFBQWEsaUJBQTNHO0FBQ0E7QUF4QkY7QUEwQkE7QUFsQ0Q7QUFvQ0E7QUF0Q0Q7QUF3Q0EsR0ExQ0Q7QUEyQ0E7O0FBRUQsUUFBTyxZQUFQO0FBQ0E7O0FBRUQsU0FBUyxrQkFBVCxDQUE0QixlQUE1QixFQUE2QztBQUM1QyxLQUFNLGVBQWUsRUFBckI7QUFDQSxLQUFNLGdCQUFnQixNQUFNLElBQU4sQ0FBVyxlQUFYLENBQXRCOztBQUVBLGVBQWMsR0FBZCxDQUFrQixVQUFDLFFBQUQsRUFBYztBQUMvQixNQUFNLFVBQVUsU0FBUyxPQUF6QjtBQUNBLGVBQWEsT0FBYixJQUF3QixFQUF4QjtBQUNBLE1BQU0sWUFBWSxNQUFNLElBQU4sQ0FBVyxTQUFTLEtBQXBCLENBQWxCO0FBQ0EsWUFBVSxHQUFWLENBQWMsVUFBQyxLQUFELEVBQVc7QUFDeEIsT0FBSSxLQUFKLEVBQVc7QUFDVixpQkFBYSxPQUFiLEVBQXNCLE1BQU0sUUFBTixFQUF0QixJQUEwQyxTQUFTLEtBQVQsQ0FBZSxNQUFNLFFBQU4sRUFBZixDQUExQztBQUNBO0FBQ0QsR0FKRDtBQUtBLEVBVEQ7O0FBV0EsUUFBTyxZQUFQO0FBQ0E7O0FBR0QsU0FBUyxZQUFULENBQXNCLElBQXRCLEVBQTRCLE9BQTVCLEVBQXFDO0FBQ3BDLE1BQUssTUFBTCxHQUFjLElBQWQ7QUFDQSxNQUFLLE9BQUwsR0FBZSxPQUFmO0FBQ0EsTUFBSyxRQUFMLEdBQWdCLFlBQVc7QUFDMUIsU0FBTyxJQUFQO0FBQ0EsRUFGRDtBQUdBOzs7OztBQ25SRDs7Ozs7O0FBRUEsUUFBUSxJQUFSLENBQWEsU0FBYjtBQUNBLFFBQVEsR0FBUixDQUFZLDBCQUFXLEVBQUMsT0FBTyxNQUFSLEVBQWdCLFVBQVUsV0FBMUIsRUFBWCxFQUFtRCxRQUEvRDtBQUNBLFFBQVEsR0FBUixDQUFZLDBCQUFXLEVBQUMsU0FBUyxHQUFWLEVBQWUsT0FBTyxTQUF0QixFQUFpQyxVQUFVLE9BQTNDLEVBQVgsQ0FBWjtBQUNBLFFBQVEsR0FBUixDQUFZLDBCQUFXLEVBQUMsU0FBUyxJQUFWLEVBQVgsQ0FBWjtBQUNBLFFBQVEsR0FBUixDQUFZLDBCQUFXLEVBQUMsT0FBTyxNQUFSLEVBQWdCLFVBQVUsT0FBMUIsRUFBWCxDQUFaO0FBQ0EsUUFBUSxHQUFSLENBQVksMEJBQVcsRUFBQyxPQUFPLFNBQVIsRUFBbUIsVUFBVSxPQUE3QixFQUFYLENBQVo7QUFDQSxRQUFRLEdBQVIsQ0FBWSwwQkFBVyxFQUFDLFNBQVMsRUFBVixFQUFjLE9BQU8sU0FBckIsRUFBZ0MsVUFBVSxPQUExQyxFQUFYLENBQVo7QUFDQSxRQUFRLEdBQVIsQ0FBWSwwQkFBVyxFQUFDLE9BQU8sU0FBUixFQUFtQixVQUFVLE9BQTdCLEVBQXNDLFdBQVcsSUFBakQsRUFBWCxDQUFaO0FBQ0EsUUFBUSxPQUFSLENBQWdCLFNBQWhCOztBQUVBO0FBQ0E7QUFDQTtBQUNBIiwiZmlsZSI6ImdlbmVyYXRlZC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzQ29udGVudCI6WyIoZnVuY3Rpb24gZSh0LG4scil7ZnVuY3Rpb24gcyhvLHUpe2lmKCFuW29dKXtpZighdFtvXSl7dmFyIGE9dHlwZW9mIHJlcXVpcmU9PVwiZnVuY3Rpb25cIiYmcmVxdWlyZTtpZighdSYmYSlyZXR1cm4gYShvLCEwKTtpZihpKXJldHVybiBpKG8sITApO3ZhciBmPW5ldyBFcnJvcihcIkNhbm5vdCBmaW5kIG1vZHVsZSAnXCIrbytcIidcIik7dGhyb3cgZi5jb2RlPVwiTU9EVUxFX05PVF9GT1VORFwiLGZ9dmFyIGw9bltvXT17ZXhwb3J0czp7fX07dFtvXVswXS5jYWxsKGwuZXhwb3J0cyxmdW5jdGlvbihlKXt2YXIgbj10W29dWzFdW2VdO3JldHVybiBzKG4/bjplKX0sbCxsLmV4cG9ydHMsZSx0LG4scil9cmV0dXJuIG5bb10uZXhwb3J0c312YXIgaT10eXBlb2YgcmVxdWlyZT09XCJmdW5jdGlvblwiJiZyZXF1aXJlO2Zvcih2YXIgbz0wO288ci5sZW5ndGg7bysrKXMocltvXSk7cmV0dXJuIHN9KSIsIkFycmF5LnByb3RvdHlwZS5leHRlbmRNYXAgPSBmdW5jdGlvbihjYWxsYmFjaywgdGhpc0FyZykge1xyXG5cdGxldCBULCBrO1xyXG5cclxuXHRpZiAodGhpcyA9PSBudWxsKSB7XHJcblx0XHR0aHJvdyBuZXcgVHlwZUVycm9yKCcgdGhpcyBpcyBudWxsIG9yIG5vdCBkZWZpbmVkJyk7XHJcblx0fVxyXG5cclxuXHRjb25zdCBPID0gT2JqZWN0KHRoaXMpO1xyXG5cdGNvbnN0IGxlbiA9IE8ubGVuZ3RoID4+PiAwO1xyXG5cclxuXHRpZiAodHlwZW9mIGNhbGxiYWNrICE9PSAnZnVuY3Rpb24nKSB7XHJcblx0XHR0aHJvdyBuZXcgVHlwZUVycm9yKGNhbGxiYWNrICsgJyBpcyBub3QgYSBmdW5jdGlvbicpO1xyXG5cdH1cclxuXHJcblx0aWYgKGFyZ3VtZW50cy5sZW5ndGggPiAxKSB7XHJcblx0XHRUID0gdGhpc0FyZztcclxuXHR9XHJcblxyXG5cdGNvbnN0IEEgPSBuZXcgQXJyYXkobGVuKTtcclxuXHRrID0gMDtcclxuXHJcblx0d2hpbGUgKGsgPCB0aGlzLmxlbmd0aCkge1xyXG5cdFx0bGV0IGtWYWx1ZSwgbWFwcGVkVmFsdWU7XHJcblxyXG5cdFx0aWYgKGsgaW4gTykge1xyXG5cdFx0XHRrVmFsdWUgPSBPW2tdO1xyXG5cdFx0XHRtYXBwZWRWYWx1ZSA9IGNhbGxiYWNrLmNhbGwoVCwga1ZhbHVlLCBrLCBPKTtcclxuXHRcdFx0QVtrXSA9IG1hcHBlZFZhbHVlO1xyXG5cdFx0fVxyXG5cclxuXHRcdGsrKztcclxuXHR9XHJcblxyXG5cdHJldHVybiBBO1xyXG59O1xyXG5cclxuXHJcbmV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIEV4dHJhY3RDU1MoaW5wdXRzKSB7XHJcbi8qXHRpbnB1dHMgPSBPYmplY3QuYXNzaWduKHtcclxuXHRcdGVsZW1lbnQ6IHVuZGVmaW5lZCxcclxuXHRcdGNsYXNzOiB1bmRlZmluZWQsXHJcblx0XHRpZDogdW5kZWZpbmVkLFxyXG5cdFx0cHJvcGVydHk6IHVuZGVmaW5lZCxcclxuXHRcdGZlYXRoZXI6IGZhbHNlXHJcblx0fSwgaW5wdXRzKTtcclxuKi9cclxuXHJcblx0dHJ5IHtcclxuXHRcdGNvbnN0IHNoZWV0cyA9IEFycmF5LmZyb20oZG9jdW1lbnQuc3R5bGVTaGVldHMpO1xyXG5cdFx0Y29uc3QgYW5pbWF0aW9ucyA9IHt9O1xyXG5cdFx0Y29uc3QgcmVzdWx0cyA9IFtdO1xyXG5cdFx0Y29uc3Qgd2VpZ2h0ZWRSZXN1bHRzID0ge307XHJcblxyXG5cdFx0c2hlZXRzLm1hcCgoc2hlZXQpID0+IHtcclxuXHRcdFx0aWYgKCFzaGVldC5jc3NSdWxlcykgdGhyb3cgbmV3IEV4dHJhY3RFcnJvcig0MDUsIFwiTm8gQ1NTIFJ1bGVzIVxcbkluIGNlcnRhaW4gYnJvd3NlcnMsIENTUyBydWxlcyBjYW5ub3QgYmUgYWNjZXNzZWQgaWYgdGhleSBhcmUgbG9hZGVkIGZyb20gbG9jYWwgZGlzayBvciBjcm9zcyBvcmlnaW4uXCIpO1xyXG5cdFx0XHRjb25zdCBjc3NTaGVldCA9IEFycmF5LmZyb20oc2hlZXQuY3NzUnVsZXMpO1xyXG5cdFx0XHRjc3NTaGVldC5leHRlbmRNYXAoKHJ1bGUpID0+IHtcclxuXHRcdFx0XHRjb25zdCBwYXJzZVRleHQgPSBydWxlLnNlbGVjdG9yVGV4dDtcclxuXHJcblx0XHRcdFx0aWYgKHJ1bGUudHlwZSA9PT0gNCkge1xyXG5cdFx0XHRcdFx0Ly8gQG1lZGlhXHJcblx0XHRcdFx0XHRjc3NTaGVldC5wdXNoKC4uLkFycmF5LmZyb20ocnVsZS5jc3NSdWxlcykpO1xyXG5cdFx0XHRcdH1cclxuXHJcblx0XHRcdFx0aWYgKHJ1bGUudHlwZSA9PT0gNykge1xyXG5cdFx0XHRcdFx0Ly8gQGtleWZyYW1lc1xyXG5cdFx0XHRcdFx0YW5pbWF0aW9uc1tydWxlLm5hbWVdID0gcnVsZS5jc3NSdWxlcztcclxuXHRcdFx0XHR9XHJcblxyXG5cdFx0XHRcdGlmIChwYXJzZVRleHQpIHtcclxuXHRcdFx0XHRcdGNvbnN0IHNlbGVjdG9ycyA9IHJ1bGUuc2VsZWN0b3JUZXh0LnNwbGl0KCcgJyk7XHJcblx0XHRcdFx0XHRzZWxlY3RvcnMubWFwKChzZWxlY3RvcikgPT4ge1xyXG5cdFx0XHRcdFx0XHRjb25zdCBzZWFyY2hPYmplY3QgPSBQYXJzZUNTUyhzZWxlY3Rvcik7XHJcblx0XHRcdFx0XHRcdHNlYXJjaE9iamVjdC5zdHlsZXMgPSB7fTtcclxuXHJcblx0XHRcdFx0XHRcdGlmIChydWxlLnBhcmVudFJ1bGUpIHtcclxuXHRcdFx0XHRcdFx0XHQvLyBJZiBAbWVkaWEgYWRkIGNvbmRpdGlvbnMgdG8gb2JqZWN0XHJcblx0XHRcdFx0XHRcdFx0aWYgKHNlYXJjaE9iamVjdC5jb25kaXRpb25zKSB7XHJcblx0XHRcdFx0XHRcdFx0XHRzZWFyY2hPYmplY3QuY29uZGl0aW9ucy5wdXNoKHJ1bGUucGFyZW50UnVsZS5jb25kaXRpb25UZXh0LnNwbGl0KC8oPzphbmQpfFxcc3xcXCgoLispXFwpLykuZmlsdGVyKChhKSA9PiB7IGlmIChhKSByZXR1cm4gYTsgfSkpO1xyXG5cdFx0XHRcdFx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0XHRcdFx0XHRzZWFyY2hPYmplY3QuY29uZGl0aW9ucyA9IHJ1bGUucGFyZW50UnVsZS5jb25kaXRpb25UZXh0LnNwbGl0KC8oPzphbmQpfFxcc3xcXCgoLispXFwpLykuZmlsdGVyKChhKSA9PiB7IGlmIChhKSByZXR1cm4gYTsgfSk7XHJcblx0XHRcdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0XHR9XHJcblxyXG5cdFx0XHRcdFx0XHRsZXQgYU1hdGNoID0gdW5kZWZpbmVkO1xyXG5cdFx0XHRcdFx0XHRPYmplY3Qua2V5cyhpbnB1dHMpLm1hcCgoa2V5KSA9PiB7XHJcblx0XHRcdFx0XHRcdFx0aWYgKFtcImVsZW1lbnRcIiwgXCJjbGFzc1wiLCBcImlkXCIsIFwicHNldWRvZWxlbWVudFwiLCBcInBzZXVkb2NsYXNzXCJdLmluY2x1ZGVzKGtleS50b0xvd2VyQ2FzZSgpKSAmJiAoYU1hdGNoID09PSB1bmRlZmluZWQgfHwgYU1hdGNoKSkge1xyXG5cdFx0XHRcdFx0XHRcdFx0aWYgKGlucHV0c1trZXldID09PSBzZWFyY2hPYmplY3Rba2V5XSB8fCAoaW5wdXRzW2tleV0gPT09IFwiXCIgJiYgIXNlYXJjaE9iamVjdFtrZXldKSkgYU1hdGNoID0gdHJ1ZTtcclxuXHRcdFx0XHRcdFx0XHRcdGVsc2UgYU1hdGNoID0gZmFsc2U7XHJcblx0XHRcdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0XHR9KTtcclxuXHJcblx0XHRcdFx0XHRcdGxldCB3ZWlnaHQgPSAwO1xyXG5cdFx0XHRcdFx0XHRpZiAoaW5wdXRzLmZlYXRoZXIgJiYgIWFNYXRjaCkge1xyXG5cdFx0XHRcdFx0XHRcdGxldCBtdWx0aSA9IDA7XHJcblxyXG5cdFx0XHRcdFx0XHRcdChpbnB1dHMuZWxlbWVudCA9PT0gc2VhcmNoT2JqZWN0LmVsZW1lbnQgJiYgaW5wdXRzLmVsZW1lbnQgIT0gXCJcIiAmJiAhc2VhcmNoT2JqZWN0LmNsYXNzKSA/IHdlaWdodCArPSAxIDogbnVsbDtcclxuXHRcdFx0XHRcdFx0XHQoaW5wdXRzLmNsYXNzICYmIGlucHV0cy5jbGFzcyA9PT0gc2VhcmNoT2JqZWN0LmNsYXNzKSA/IG11bHRpICs9IDEgOiBudWxsO1xyXG5cdFx0XHRcdFx0XHRcdChpbnB1dHMuaWQgJiYgaW5wdXRzLmlkID09PSBzZWFyY2hPYmplY3QuaWQpID8gbXVsdGkgKz0gMSA6IG51bGw7XHJcblxyXG5cdFx0XHRcdFx0XHRcdGlmICgoaW5wdXRzLmNsYXNzICYmIGlucHV0cy5jbGFzcyA9PT0gc2VhcmNoT2JqZWN0LmNsYXNzKSB8fCAoaW5wdXRzLmlkICYmIGlucHV0cy5pZCA9PT0gc2VhcmNoT2JqZWN0LmlkKSkge1xyXG5cdFx0XHRcdFx0XHRcdFx0d2VpZ2h0ICs9IDMgKiBtdWx0aTtcclxuXHRcdFx0XHRcdFx0XHRcdChpbnB1dHMuZWxlbWVudCA9PT0gc2VhcmNoT2JqZWN0LmVsZW1lbnQpID8gd2VpZ2h0ICs9IDYgKiBtdWx0aSA6IG51bGw7XHJcblx0XHRcdFx0XHRcdFx0XHRpZiAoaW5wdXRzLmVsZW1lbnQgIT0gc2VhcmNoT2JqZWN0LmVsZW1lbnQpIHtcclxuXHRcdFx0XHRcdFx0XHRcdFx0KHNlYXJjaE9iamVjdC5lbGVtZW50ID09PSBcIlwiICYmIGlucHV0cy5lbGVtZW50ICE9IFwiXCIpID8gd2VpZ2h0ICs9IDIgKiBtdWx0aSA6IG51bGw7XHJcblx0XHRcdFx0XHRcdFx0XHRcdChzZWFyY2hPYmplY3QuZWxlbWVudCAhPSBcIlwiICYmIGlucHV0cy5lbGVtZW50ID09PSBcIlwiKSA/IHdlaWdodCArPSAxICogbXVsdGkgOiBudWxsO1xyXG5cdFx0XHRcdFx0XHRcdFx0fVxyXG5cclxuXHRcdFx0XHRcdFx0XHRcdGlmIChpbnB1dHMucHNldWRvRWxlbWVudCkge1xyXG5cdFx0XHRcdFx0XHRcdFx0XHQoaW5wdXRzLnBzZXVkb0VsZW1lbnQgPT09IHNlYXJjaE9iamVjdC5wc2V1ZG9FbGVtZW50KSA/IHdlaWdodCArPSAoMiAqIG11bHRpKSArIDEgOiBudWxsO1xyXG5cdFx0XHRcdFx0XHRcdFx0XHQoIXNlYXJjaE9iamVjdC5wc2V1ZG9FbGVtZW50KSA/IHdlaWdodCArPSAxICogbXVsdGkgOiBudWxsO1xyXG5cdFx0XHRcdFx0XHRcdFx0fVxyXG5cclxuXHRcdFx0XHRcdFx0XHRcdGlmIChpbnB1dHMucHNldWRvQ2xhc3MpIHtcclxuXHRcdFx0XHRcdFx0XHRcdFx0KGlucHV0cy5wc2V1ZG9DbGFzcyA9PT0gc2VhcmNoT2JqZWN0LnBzZXVkb0NsYXNzKSA/IHdlaWdodCArPSAoMiAqIG11bHRpKSArIDEgOiBudWxsO1xyXG5cdFx0XHRcdFx0XHRcdFx0XHQoIXNlYXJjaE9iamVjdC5wc2V1ZG9DbGFzcykgPyB3ZWlnaHQgKz0gMSAqIG11bHRpIDogbnVsbDtcclxuXHRcdFx0XHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRcdH1cclxuXHJcblx0XHRcdFx0XHRcdGlmIChhTWF0Y2ggfHwgd2VpZ2h0ID4gMCkge1xyXG5cdFx0XHRcdFx0XHRcdEFycmF5LmZyb20ocnVsZS5zdHlsZSkubWFwKChzdHlsZSkgPT4ge1xyXG5cdFx0XHRcdFx0XHRcdFx0Ly8gR2F0aGVyIHJlZ2lzdGVyZWQgc3R5bGVzXHJcblx0XHRcdFx0XHRcdFx0XHRpZiAoc3R5bGUgJiYgc3R5bGUudG9TdHJpbmcoKS5zcGxpdCgnLScpWzBdICE9IFwiXCIpIHtcclxuXHRcdFx0XHRcdFx0XHRcdFx0aWYgKFtcImFuaW1hdGlvblwiLCBcImJvcmRlclwiLCBcIm1hcmdpblwiLCBcInBhZGRpbmdcIiwgXCJiYWNrZ3JvdW5kXCJdLmluY2x1ZGVzKHN0eWxlLnRvU3RyaW5nKCkuc3BsaXQoJy0nKVswXSkpIHtcclxuXHRcdFx0XHRcdFx0XHRcdFx0XHRzZWFyY2hPYmplY3Quc3R5bGVzW3N0eWxlLnRvU3RyaW5nKCkuc3BsaXQoJy0nKVswXV0gPSBydWxlLnN0eWxlW3N0eWxlLnRvU3RyaW5nKCkuc3BsaXQoJy0nKVswXV07XHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0aWYgKHN0eWxlLnRvU3RyaW5nKCkgPT09IFwiYW5pbWF0aW9uLW5hbWVcIikge1xyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0Ly8gRm9yIGFuaW1hdGlvbnMsIGdhdGhlciB0aGUgcmVnaXN0ZXJlZCBAa2V5ZnJhbWVzXHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRpZiAoYW5pbWF0aW9uc1tydWxlLnN0eWxlW3N0eWxlLnRvU3RyaW5nKCldXSkge1xyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRzZWFyY2hPYmplY3Quc3R5bGVzLmFuaW1hdGlvbkZyYW1lcyA9IEJyZWFrZG93bkFuaW1hdGlvbihhbmltYXRpb25zW3J1bGUuc3R5bGVbc3R5bGUudG9TdHJpbmcoKV1dKTtcclxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdGlmIChpbnB1dHMucHJvcGVydHkgJiYgaW5wdXRzLnByb3BlcnR5ID09PSBcImFuaW1hdGlvblwiKSB7XHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0dGhyb3cgbmV3IEV4dHJhY3RFcnJvcig0MDQsIFwiQW5pbWF0aW9uIGhhcyBubyBmcmFtZXNcIik7XHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0c2VhcmNoT2JqZWN0LnN0eWxlcy5hbmltYXRpb25GcmFtZXMgPSB7NDA0OiBcIkFuaW1hdGlvbiBoYXMgbm8gZnJhbWVzXCJ9O1xyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0XHRcdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdHNlYXJjaE9iamVjdC5zdHlsZXNbc3R5bGUudG9TdHJpbmcoKV0gPSBydWxlLnN0eWxlW3N0eWxlLnRvU3RyaW5nKCldO1xyXG5cdFx0XHRcdFx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRcdFx0fSk7XHJcblxyXG5cdFx0XHRcdFx0XHRcdGlmIChpbnB1dHMucHJvcGVydHkpIHtcclxuXHRcdFx0XHRcdFx0XHRcdGlmICghc2VhcmNoT2JqZWN0LnN0eWxlc1tpbnB1dHMucHJvcGVydHkudG9Mb3dlckNhc2UoKV0pIHtcclxuXHRcdFx0XHRcdFx0XHRcdFx0YU1hdGNoID0gZmFsc2U7XHJcblx0XHRcdFx0XHRcdFx0XHRcdHdlaWdodCA9IDA7XHJcblx0XHRcdFx0XHRcdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdFx0XHRcdFx0XHRzZWFyY2hPYmplY3QucHJvcGVydHkgPSB7fTtcclxuXHRcdFx0XHRcdFx0XHRcdFx0c2VhcmNoT2JqZWN0LnByb3BlcnR5W2lucHV0cy5wcm9wZXJ0eS50b0xvd2VyQ2FzZSgpXSA9IHNlYXJjaE9iamVjdC5zdHlsZXNbaW5wdXRzLnByb3BlcnR5LnRvTG93ZXJDYXNlKCldO1xyXG5cdFx0XHRcdFx0XHRcdFx0XHRpZiAoaW5wdXRzLnByb3BlcnR5LnRvTG93ZXJDYXNlKCkgPT09IFwiYW5pbWF0aW9uXCIpIHtcclxuXHRcdFx0XHRcdFx0XHRcdFx0XHRzZWFyY2hPYmplY3QucHJvcGVydHkuYW5pbWF0aW9uRnJhbWVzID0gc2VhcmNoT2JqZWN0LnN0eWxlcy5hbmltYXRpb25GcmFtZXM7XHJcblx0XHRcdFx0XHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRcdH1cclxuXHJcblx0XHRcdFx0XHRcdGlmIChhTWF0Y2gpIHtcclxuXHRcdFx0XHRcdFx0XHRyZXN1bHRzLnB1c2goc2VhcmNoT2JqZWN0KTtcclxuXHRcdFx0XHRcdFx0fSBlbHNlIGlmICh3ZWlnaHQgPiAwKSB7XHJcblx0XHRcdFx0XHRcdFx0d2VpZ2h0ZWRSZXN1bHRzW3dlaWdodF0gPSBzZWFyY2hPYmplY3Q7XHJcblx0XHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdH0pO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fSk7XHJcblx0XHR9KTtcclxuXHJcblx0XHRpZiAocmVzdWx0cy5sZW5ndGggPT09IDApIHtcclxuXHRcdFx0Y29uc3Qgd2VpZ2h0ZWRMaXN0ID0gT2JqZWN0LmtleXMod2VpZ2h0ZWRSZXN1bHRzKTtcclxuXHRcdFx0d2VpZ2h0ZWRMaXN0LnNvcnQoZnVuY3Rpb24oYSwgYikge3JldHVybiBiIC0gYTt9KTtcclxuXHRcdFx0Y29uc3Qgd2VpZ2h0ZWRGaW5hbCA9IFtdO1xyXG5cdFx0XHR3ZWlnaHRlZExpc3QubWFwKCh3ZWlnaHQpID0+IHtcclxuXHRcdFx0XHR3ZWlnaHRlZEZpbmFsLnB1c2god2VpZ2h0ZWRSZXN1bHRzW3dlaWdodF0pO1xyXG5cdFx0XHR9KTtcclxuXHRcdFx0aWYgKHdlaWdodGVkRmluYWwubGVuZ3RoID09PSAwIHx8ICFpbnB1dHMuZmVhdGhlcikge1xyXG5cdFx0XHRcdHJldHVybiB1bmRlZmluZWQ7XHJcblx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0aWYgKHdlaWdodGVkRmluYWwubGVuZ3RoID09PSAxIHx8IGlucHV0cy5maXJzdE9ubHkpIHtcclxuXHRcdFx0XHRcdHJldHVybiB3ZWlnaHRlZEZpbmFsWzBdO1xyXG5cdFx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0XHRyZXR1cm4gd2VpZ2h0ZWRGaW5hbDtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH1cclxuXHRcdH0gZWxzZSBpZiAocmVzdWx0cy5sZW5ndGggPT09IDEgfHwgaW5wdXRzLmZpcnN0T25seSkge1xyXG5cdFx0XHRyZXR1cm4gcmVzdWx0c1swXTtcclxuXHRcdH0gZWxzZSB7XHJcblx0XHRcdHJldHVybiByZXN1bHRzO1xyXG5cdFx0fVxyXG5cclxuXHR9XHJcblx0Y2F0Y2goZSkge1xyXG5cdFx0Y29uc29sZS5lcnJvcihgJHtlLnN0YXR1c306ICR7ZS5tZXNzYWdlfWApO1xyXG5cdFx0cmV0dXJuIHtzdGF0dXM6IGUuc3RhdHVzLCBtZXNzYWdlOiBlLm1lc3NhZ2V9O1xyXG5cdH1cclxufVxyXG5cclxuZnVuY3Rpb24gUGFyc2VDU1MoaW5wdXQpIHtcclxuXHRjb25zdCBjc3MgPSBpbnB1dC5zcGxpdCgvKDo6fFxcWy4rXFxdfCx8OnxcXHN8XFwufCMpLyk7XHJcblx0Y29uc3Qgc2VhcmNoT2JqZWN0ID0ge307XHJcblxyXG5cdGlmIChjc3MpIHtcclxuXHRcdGNzcy5tYXAoKHNlZ21lbnQsIGluZGV4KSA9PiB7XHJcblx0XHRcdGxldCBhdHRyaWIgPSBbXTtcclxuXHRcdFx0c3dpdGNoIChpbmRleCAlIDIpIHtcclxuXHRcdFx0Y2FzZSAwOlxyXG5cdFx0XHRcdHN3aXRjaCAoaW5kZXgpIHtcclxuXHRcdFx0XHRjYXNlIDA6XHJcblx0XHRcdFx0XHQvLyBGaXJzdCBzZWdtZW50LCBpZiBub3QgZW1wdHksIHdpbGwgYWx3YXlzIGJlIHRoZSBlbGVtZW50LlxyXG5cdFx0XHRcdFx0c2VhcmNoT2JqZWN0LmVsZW1lbnQgPSBzZWdtZW50O1xyXG5cdFx0XHRcdFx0YnJlYWs7XHJcblx0XHRcdFx0ZGVmYXVsdDpcclxuXHRcdFx0XHRcdC8vIEV2ZW4gc2VnbWVudHMsIHRoYXQgYXJlIG5vdCBmaXJzdCwgY2FuIGJlIGEgdmFyaWV0eSBvZiB0aGluZ3M7XHJcblx0XHRcdFx0XHQvLyBjbGFzcywgaWQsIHBzZXVkby1jbGFzcywgcHNldWRvLWVsZW1lbnRcclxuXHRcdFx0XHRcdHN3aXRjaCAoY3NzW2luZGV4IC0gMV0pIHtcclxuXHRcdFx0XHRcdGNhc2UgXCIsXCI6XHJcblx0XHRcdFx0XHRcdC8vIHN0YW5kYXJkIHNlcGFyYXRvcjsgbm90IGNvdW50ZWQuXHJcblx0XHRcdFx0XHRcdGJyZWFrO1xyXG5cdFx0XHRcdFx0Y2FzZSBcIi5cIjpcclxuXHRcdFx0XHRcdFx0Ly8gY2xhc3NcclxuXHRcdFx0XHRcdFx0c2VhcmNoT2JqZWN0LmNsYXNzID0gKHNlYXJjaE9iamVjdC5jbGFzcyA9PT0gdW5kZWZpbmVkKSA/IHNlZ21lbnQgOiBzZWFyY2hPYmplY3QuY2xhc3M7XHJcblx0XHRcdFx0XHRcdGJyZWFrO1xyXG5cdFx0XHRcdFx0Y2FzZSBcIiNcIjpcclxuXHRcdFx0XHRcdFx0Ly8gaWRcclxuXHRcdFx0XHRcdFx0c2VhcmNoT2JqZWN0LmlkID0gKHNlYXJjaE9iamVjdC5pZCA9PT0gdW5kZWZpbmVkKSA/IHNlZ21lbnQgOiBzZWFyY2hPYmplY3QuaWQ7XHJcblx0XHRcdFx0XHRcdGJyZWFrO1xyXG5cdFx0XHRcdFx0Y2FzZSBcIjpcIjpcclxuXHRcdFx0XHRcdFx0Ly8gcHNldWRvLWNsYXNzXHJcblx0XHRcdFx0XHRcdHNlYXJjaE9iamVjdC5wc2V1ZG9DbGFzcyA9IChzZWFyY2hPYmplY3QucHNldWRvQ2xhc3MgPT09IHVuZGVmaW5lZCkgPyBzZWdtZW50IDogc2VhcmNoT2JqZWN0LnBzZXVkb0NsYXNzO1xyXG5cdFx0XHRcdFx0XHRicmVhaztcclxuXHRcdFx0XHRcdGNhc2UgXCI6OlwiOlxyXG5cdFx0XHRcdFx0XHQvLyBwc2V1ZG8tZWxlbWVudFxyXG5cdFx0XHRcdFx0XHRzZWFyY2hPYmplY3QucHNldWRvRWxlbWVudCA9IChzZWFyY2hPYmplY3QucHNldWRvRWxlbWVudCA9PT0gdW5kZWZpbmVkKSA/IHNlZ21lbnQgOiBzZWFyY2hPYmplY3QucHNldWRvRWxlbWVudDtcclxuXHRcdFx0XHRcdFx0YnJlYWs7XHJcblx0XHRcdFx0XHRkZWZhdWx0OlxyXG5cdFx0XHRcdFx0XHRhdHRyaWIgPSBjc3NbaW5kZXggLSAxXS5zcGxpdCgvKFtcXFtcXF1dKS8pO1xyXG5cdFx0XHRcdFx0XHRpZiAoYXR0cmliWzFdID09PSBcIltcIiAmJiBhdHRyaWJbM10gPT09IFwiXVwiKSB7XHJcblx0XHRcdFx0XHRcdFx0c2VhcmNoT2JqZWN0LmF0dHJpYnV0ZVNlbGVjdG9yID0gKHNlYXJjaE9iamVjdC5hdHRyaWJ1dGVTZWxlY3RvciA9PT0gdW5kZWZpbmVkKSA/IGF0dHJpYlsyXSA6IHNlYXJjaE9iamVjdC5hdHRyaWJ1dGVTZWxlY3RvcjtcclxuXHRcdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0YnJlYWs7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHRcdGJyZWFrO1xyXG5cdFx0XHR9XHJcblx0XHR9KTtcclxuXHR9XHJcblxyXG5cdHJldHVybiBzZWFyY2hPYmplY3Q7XHJcbn1cclxuXHJcbmZ1bmN0aW9uIEJyZWFrZG93bkFuaW1hdGlvbihhbmltYXRpb25PYmplY3QpIHtcclxuXHRjb25zdCBhbmltYXRpb25TZXQgPSB7fTtcclxuXHRjb25zdCBhbmltYXRpb25MaXN0ID0gQXJyYXkuZnJvbShhbmltYXRpb25PYmplY3QpO1xyXG5cclxuXHRhbmltYXRpb25MaXN0Lm1hcCgoa2V5RnJhbWUpID0+IHtcclxuXHRcdGNvbnN0IGtleVRleHQgPSBrZXlGcmFtZS5rZXlUZXh0O1xyXG5cdFx0YW5pbWF0aW9uU2V0W2tleVRleHRdID0ge307XHJcblx0XHRjb25zdCBrZXlTdHlsZXMgPSBBcnJheS5mcm9tKGtleUZyYW1lLnN0eWxlKTtcclxuXHRcdGtleVN0eWxlcy5tYXAoKHN0eWxlKSA9PiB7XHJcblx0XHRcdGlmIChzdHlsZSkge1xyXG5cdFx0XHRcdGFuaW1hdGlvblNldFtrZXlUZXh0XVtzdHlsZS50b1N0cmluZygpXSA9IGtleUZyYW1lLnN0eWxlW3N0eWxlLnRvU3RyaW5nKCldO1xyXG5cdFx0XHR9XHJcblx0XHR9KTtcclxuXHR9KTtcclxuXHJcblx0cmV0dXJuIGFuaW1hdGlvblNldDtcclxufVxyXG5cclxuXHJcbmZ1bmN0aW9uIEV4dHJhY3RFcnJvcihjb2RlLCBtZXNzYWdlKSB7XHJcblx0dGhpcy5zdGF0dXMgPSBjb2RlO1xyXG5cdHRoaXMubWVzc2FnZSA9IG1lc3NhZ2U7XHJcblx0dGhpcy50b1N0cmluZyA9IGZ1bmN0aW9uKCkge1xyXG5cdFx0cmV0dXJuIHRoaXM7XHJcblx0fTtcclxufSIsImltcG9ydCBFeHRyYWN0Q1NTIGZyb20gJy4vc3JjL2V4dHJhY3QtY3NzJztcclxuXHJcbmNvbnNvbGUudGltZSgnRXh0cmFjdCcpO1xyXG5jb25zb2xlLmRpcihFeHRyYWN0Q1NTKHtjbGFzczogXCJyb2xsXCIsIHByb3BlcnR5OiBcImFuaW1hdGlvblwifSkucHJvcGVydHkpO1xyXG5jb25zb2xlLmRpcihFeHRyYWN0Q1NTKHtlbGVtZW50OiBcInBcIiwgY2xhc3M6IFwicGtsYXNzZVwiLCBwcm9wZXJ0eTogXCJjb2xvclwifSkpO1xyXG5jb25zb2xlLmRpcihFeHRyYWN0Q1NTKHtlbGVtZW50OiBcImgyXCJ9KSk7XHJcbmNvbnNvbGUuZGlyKEV4dHJhY3RDU1Moe2NsYXNzOiBcInJ5bGxcIiwgcHJvcGVydHk6IFwid2lkdGhcIn0pKTtcclxuY29uc29sZS5kaXIoRXh0cmFjdENTUyh7Y2xhc3M6IFwicGtsYXNzZVwiLCBwcm9wZXJ0eTogXCJjb2xvclwifSkpO1xyXG5jb25zb2xlLmRpcihFeHRyYWN0Q1NTKHtlbGVtZW50OiBcIlwiLCBjbGFzczogXCJwa2xhc3NlXCIsIHByb3BlcnR5OiBcImNvbG9yXCJ9KSk7XHJcbmNvbnNvbGUuZGlyKEV4dHJhY3RDU1Moe2NsYXNzOiBcInBrbGFzc2VcIiwgcHJvcGVydHk6IFwiY29sb3JcIiwgZmlyc3RPbmx5OiB0cnVlfSkpO1xyXG5jb25zb2xlLnRpbWVFbmQoJ0V4dHJhY3QnKTtcclxuXHJcbi8vY29uc29sZS50aW1lKCdGZWF0aGVyJyk7XHJcbi8vY29uc29sZS5kaXIoRXh0cmFjdENTUyh7ZWxlbWVudDogXCJkaXZcIiwgcHNldWRvRWxlbWVudDogXCJmaXJzdC1sZXR0ZXJcIiwgcHJvcGVydHk6IFwiY29sb3JcIiwgZmVhdGhlcjogdHJ1ZX0pLnByb3BlcnR5KTtcclxuLy9jb25zb2xlLmRpcihFeHRyYWN0Q1NTKHtlbGVtZW50OiBcImRpdlwiLCBjbGFzczogXCJmdGVzdFwiLCBwc2V1ZG9FbGVtZW50OiBcImZpcnN0LWxldHRlclwiLCBwcm9wZXJ0eTogXCJjb2xvclwiLCBmZWF0aGVyOiB0cnVlfSlbMF0ucHJvcGVydHkpO1xyXG4vL2NvbnNvbGUudGltZUVuZCgnRmVhdGhlcicpO1xyXG4iXX0=
